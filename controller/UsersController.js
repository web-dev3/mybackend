const usersController = {
  userList: [
    { id: 1, name: 'Thinnait', gender: 'M' },
    { id: 2, name: 'Prasob', gender: 'M' }
  ],
  lastId: 3,
  addUser(user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
    return { id }
  },
  getUsers () {
    return [...this.userList]
  },
  getUser (id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}

module.exports = usersController